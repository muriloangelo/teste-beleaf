import Vue from "vue";
import VueRouter from "vue-router";

const AdminLayout = () => import("@/layouts/AdminLayout");
const Painel = () => import("@/views/Painel");

import { authRoutes } from "@/views/auth";
import { settingsRoutes } from "@/views/settings";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/painel",
    name: "Painel",
    component: AdminLayout,
    children: [
      {
        path: "/painel",
        name: "Login",
        component: Painel,
      },
    ],
  },
];

const appRoutes = [...authRoutes, ...settingsRoutes, ...routes];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: appRoutes,
});

export default router;
