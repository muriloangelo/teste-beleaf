const Settings = () => import("./pages/Settings");
const AdminLayout = () => import("@/layouts/AdminLayout");

const authRoutes = [
  {
    path: "/configuracoes",
    component: AdminLayout,
    children: [
      {
        path: "/configuracoes/conta",
        name: "Settings",
        component: Settings,
      },
    ],
  },
];

export default authRoutes;
