import axios from "@/utils/axiosInstance.js";

const login = ({ username, password }) =>
  axios.post("/auth/signin", { username, password });

export default { login };
