export default {
  editingForm: ({ commit }, payload) => {
    commit("EDITING_FORM", payload);
  },
};
