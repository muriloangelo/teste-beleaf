const Login = () => import("./pages/Login");
const AuthLayout = () => import("@/layouts/AuthLayout");

const authRoutes = [
  {
    path: "/auth",
    component: AuthLayout,
    children: [
      {
        path: "/auth/login",
        name: "Login",
        component: Login,
      },
    ],
  },
];

export default authRoutes;
