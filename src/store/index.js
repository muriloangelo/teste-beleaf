import Vue from "vue";
import Vuex from "vuex";

import auth from "@/views/auth/store";
import settings from "@/views/settings/store";
import ui from "@/views/ui/store";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { auth, settings, ui },
});
