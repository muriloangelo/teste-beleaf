import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import PolarisVue from "@hulkapps/polaris-vue";

Vue.config.productionTip = false;

Vue.use(PolarisVue);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
