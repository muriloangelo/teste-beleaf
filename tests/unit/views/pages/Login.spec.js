import { shallowMount } from "@vue/test-utils";
import Login from "@/views/auth/pages/Login";

describe("Login.vue", () => {
  it("has a name", () => {
    expect(Login.name).toBe("Login");
  });
  it("is Login", () => {
    const wrapper = shallowMount(Login);
    expect(wrapper.is(Login)).toBe(true);
  });
  test("renders correctly", () => {
    const wrapper = shallowMount(Login);
    expect(wrapper.element).toMatchSnapshot();
  });
});
