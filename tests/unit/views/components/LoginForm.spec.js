import Vue from "vue";
import PolarisVue from "@hulkapps/polaris-vue";
import { shallowMount } from "@vue/test-utils";
import LoginForm from "@/views/auth/components/LoginForm";
import VueRouter from "vue-router";

Vue.use(VueRouter);
Vue.use(PolarisVue);

describe("LoginForm.vue", () => {
  it("has a name", () => {
    expect(LoginForm.name).toBe("LoginForm");
  });
  it("is LoginForm", () => {
    const wrapper = shallowMount(LoginForm);
    expect(wrapper.is(LoginForm)).toBe(true);
  });
  test("renders correctly", () => {
    const wrapper = shallowMount(LoginForm);
    expect(wrapper.element).toMatchSnapshot();
  });
});
