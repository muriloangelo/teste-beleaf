import Vue from "vue";
import { shallowMount } from "@vue/test-utils";
import AdminLayout from "@/layouts/AdminLayout";
import VueRouter from "vue-router";

Vue.use(VueRouter);

describe("AdminLayout.vue", () => {
  it("has a name", () => {
    expect(AdminLayout.name).toBe("AdminLayout");
  });
  it("is AdminLayout", () => {
    const wrapper = shallowMount(AdminLayout);
    expect(wrapper.is(AdminLayout)).toBe(true);
  });
  test("renders correctly", () => {
    const wrapper = shallowMount(AdminLayout);
    expect(wrapper.element).toMatchSnapshot();
  });
});
