import Vue from "vue";
import { shallowMount } from "@vue/test-utils";
import AuthLayout from "@/layouts/AuthLayout";
import VueRouter from "vue-router";

Vue.use(VueRouter);

describe("AuthLayout.vue", () => {
  it("has a name", () => {
    expect(AuthLayout.name).toBe("AuthLayout");
  });
  it("is AuthLayout", () => {
    const wrapper = shallowMount(AuthLayout);
    expect(wrapper.is(AuthLayout)).toBe(true);
  });
  test("renders correctly", () => {
    const wrapper = shallowMount(AuthLayout);
    expect(wrapper.element).toMatchSnapshot();
  });
});
