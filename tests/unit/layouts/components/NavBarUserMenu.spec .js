import Vue from "vue";
import { shallowMount } from "@vue/test-utils";
import NavBarUserMenu from "@/layouts/components/NavBarUserMenu";
import VueRouter from "vue-router";

Vue.use(VueRouter);

describe("NavBarUserMenu.vue", () => {
  it("has a name", () => {
    expect(NavBarUserMenu.name).toBe("NavBarUserMenu");
  });
  it("is NavBarUserMenu", () => {
    const wrapper = shallowMount(NavBarUserMenu);
    expect(wrapper.is(NavBarUserMenu)).toBe(true);
  });
  test("renders correctly", () => {
    const wrapper = shallowMount(NavBarUserMenu);
    expect(wrapper.element).toMatchSnapshot();
  });
});
