import Vue from "vue";
import { shallowMount } from "@vue/test-utils";
import NavBar from "@/layouts/components/NavBar";
import VueRouter from "vue-router";

Vue.use(VueRouter);

describe("NavBar.vue", () => {
  it("has a name", () => {
    expect(NavBar.name).toBe("NavBar");
  });
  it("is NavBar", () => {
    const wrapper = shallowMount(NavBar);
    expect(wrapper.is(NavBar)).toBe(true);
  });
  test("renders correctly", () => {
    const wrapper = shallowMount(NavBar);
    expect(wrapper.element).toMatchSnapshot();
  });
});
